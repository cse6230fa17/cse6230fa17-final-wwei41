//#include "mmma_double.h"

#include<stdio.h>
#include<stdlib.h>
/* *C *A * B point to upper left corner of subMatrix C A B
 * jumpC jumpB jumpA are num_col in the original matrix
 * m, n , r are size of matrix for the subproblem
 * C is m by n, A is m by r, B is r by n
 */
void divide_conquer(double* C, const double* A, const double* B, 
    size_t jumpC, size_t jumpA, size_t jumpB,
    size_t m, size_t n, size_t r);
// plain/humble/straightforward matrix multiplication
// argument definination is the same as devide_conquer()
void multiply(double* C, const double* A, const double* B, 
    size_t jumpC, size_t jumpA, size_t jumpB,
    size_t m, size_t n, size_t r);

int matrix_matrix_multiply_add_double (size_t m, size_t n, size_t r, double alpha, double *C, const double *A, const double *B)
{
  //scaling
  for (size_t i = 0; i < m * n; i++) {
    C[i] *= alpha;
  }
  //divide and conquer for matrix multiplication
  divide_conquer(C, A, B, n, r, n, m, n, r);
  //multiply(C, A, B, n, r, m, m , n, r);
  return 0;
}
void divide_conquer(double* C, const double* A, const double* B, 
    size_t jumpC, size_t jumpA, size_t jumpB,
    size_t m, size_t n, size_t r){
  //Once the sub matrix fit level 1 cache, perform the plain matrix multiplication
  if((m * n + m * r + r * n) < 256) {
    //fit level one cache
    multiply(C, A, B, jumpC, jumpA, jumpB, m, n , r);
  } else {
    if( m > 2 * n) {
      //tall matrix, divide vertically
      divide_conquer(C,A,B,
          jumpC, jumpA, jumpB,
          m/2, n, r);
      divide_conquer(C + m / 2 * jumpC, A + m / 2 * jumpA, B, 
          jumpC, jumpA, jumpB, 
          m - m / 2, n, r);
    } else if(n > 2 * m) {
      //fat matrix, divide horizontally
      divide_conquer(C, A, B,
          jumpC, jumpA, jumpB,
          m, n / 2, r);
      divide_conquer(C + n / 2, A, B + n / 2,
          jumpC, jumpA, jumpB, 
          m, n - n / 2, r);
    } else {
      //neither too tall nor too fat, divide quartly
      // C11 C12   A11 A12   B11 B12   A11B11 + A12B21  A11B12 + A12B22 
      //         =         *         =
      // C21 C22   A21 A22   B21 B22   A21B11 + A22B21  A21B12 + A22B22
      
      // upper left 1 A11B11
      divide_conquer(C,A,B, 
          jumpC, jumpA, jumpB, 
          m / 2, n / 2, r / 2);
      //upper left 2 A12B21
      divide_conquer(C,A + r / 2, B + r / 2 * jumpB,
          jumpC, jumpA, jumpB, 
          m / 2, n / 2, r - r / 2);

      //lower left 1 A21B12
      divide_conquer(C + m / 2 * jumpC, A + m / 2 * jumpA, B,
          jumpC, jumpA, jumpB,
          m - m / 2, n / 2, r / 2);
      //lower left 2 A22B22
      divide_conquer(C + m / 2 * jumpC, A + m / 2 * jumpA + r / 2, 
          B + r / 2 * jumpB, 
          jumpC, jumpA, jumpB,
          m - m / 2, n / 2, r - r / 2);

      //upper right 1 A11B12
      divide_conquer(C + n / 2, A, B + n / 2,
          jumpC, jumpA, jumpB,
          m / 2, n - n / 2, r / 2);
      //upper right 2 A12B22
      divide_conquer(C + n / 2, A + r / 2, B + r / 2 * jumpB + n / 2,
          jumpC, jumpA, jumpB,
          m / 2, n - n / 2, r - r / 2);

      //lower right 1 A21B12
      divide_conquer(C + m / 2 * jumpC + n / 2, A + m / 2 * jumpA, B + n / 2,
          jumpC, jumpA, jumpB,
          m - m / 2, n - n / 2, r / 2);
      //lower right 2 A22B22
      divide_conquer(C + m / 2 * jumpC + n / 2, A + m / 2 * jumpA + r / 2, 
          B + r / 2 * jumpB + n / 2,
          jumpC, jumpA, jumpB,
          m - m / 2, n - n / 2, r - r / 2);
    }
  }
}
//The humble matrix multiplication
void multiply(double* C, const double* A, const double* B, 
    size_t jumpC, size_t jumpA, size_t jumpB,
    size_t m, size_t n, size_t r){
  for(size_t i = 0; i < m; ++i) {
    for(size_t j = 0; j < n; ++j) {
      for(size_t k = 0; k < r; ++k) {
        C[i * jumpC + j] += A[i * jumpA + k] * B[k * jumpB + j];
      }
    }
  }
}


