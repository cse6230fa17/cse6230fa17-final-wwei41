
#include <petscsys.h>
#include "mmma.h"

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error");} while(0)

//-------------INSTRUCTOR MODIFICATIONS: samindaw--------------------------//
#define BLOCK_ROW 16 
#define BLOCK_COL 256
#define BLOCK_K 32
#define STRIDES 4
//-------------------------------------------------------------------------//

int MMMAGetMatrixArraysDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R,
    int numDevices,
    struct MatrixBlock cBlock[],
    struct MatrixBlock aBlock[],
    struct MatrixBlock bBlock[],
    double *CLocal_p[], double *ALocal_p[], double *BLocal_p[])
{
  PetscInt nClocal, nAlocal, nBlocal;
  cudaError_t       cerr;

  PetscFunctionBeginUser;

  cBlock[0].rowStart = 0;
  cBlock[0].rowEnd   = M;
  cBlock[0].colStart = 0;
  cBlock[0].colEnd   = N;
  nClocal = M * N;

  aBlock[0].rowStart = 0;
  aBlock[0].rowEnd   = M;
  aBlock[0].colStart = 0;
  aBlock[0].colEnd   = R;
  nAlocal = M * R;

  bBlock[0].rowStart = 0;
  bBlock[0].rowEnd   = R;
  bBlock[0].colStart = 0;
  bBlock[0].colEnd   = N;
  nBlocal = R * N;

  cerr = cudaMalloc(&CLocal_p[0], nClocal * sizeof(double)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&ALocal_p[0], nAlocal * sizeof(double)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&BLocal_p[0], nBlocal * sizeof(double)); CUDA_CHK(cerr);

  for (int i = 1; i < numDevices; i++) {
    cBlock[i].rowStart = M;
    cBlock[i].rowEnd   = M;
    cBlock[i].colStart = N;
    cBlock[i].colEnd   = N;
    aBlock[i].rowStart = M;
    aBlock[i].rowEnd   = M;
    aBlock[i].colStart = R;
    aBlock[i].colEnd   = R;
    bBlock[i].rowStart = R;
    bBlock[i].rowEnd   = R;
    bBlock[i].colStart = N;
    bBlock[i].colEnd   = N;
    CLocal_p[i] = NULL;
    ALocal_p[i] = NULL;
    BLocal_p[i] = NULL;
  }

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R,
    int numDevices,
    struct MatrixBlock cBlock[],
    struct MatrixBlock aBlock[],
    struct MatrixBlock bBlock[],
    double *CLocal_p[], double *ALocal_p[], double *BLocal_p[])
{
  cudaError_t    cerr;

  PetscFunctionBeginUser;
  cerr = cudaFree(CLocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(ALocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(BLocal_p[0]); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}

int MMMAGetMatrixArraysSingleDevice(MMMA mmma, size_t M, size_t N, size_t R,
    int numDevices,
    struct MatrixBlock cBlock[],
    struct MatrixBlock aBlock[],
    struct MatrixBlock bBlock[],
    float *CLocal_p[], float *ALocal_p[], float *BLocal_p[])
{
  PetscInt nClocal, nAlocal, nBlocal;
  cudaError_t    cerr;

  PetscFunctionBeginUser;

  cBlock[0].rowStart = 0;
  cBlock[0].rowEnd   = M;
  cBlock[0].colStart = 0;
  cBlock[0].colEnd   = N;
  nClocal = M * N;

  aBlock[0].rowStart = 0;
  aBlock[0].rowEnd   = M;
  aBlock[0].colStart = 0;
  aBlock[0].colEnd   = R;
  nAlocal = M * R;

  bBlock[0].rowStart = 0;
  bBlock[0].rowEnd   = R;
  bBlock[0].colStart = 0;
  bBlock[0].colEnd   = N;
  nBlocal = R * N;

  cerr = cudaMalloc(&CLocal_p[0], nClocal * sizeof(float)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&ALocal_p[0], nAlocal * sizeof(float)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&BLocal_p[0], nBlocal * sizeof(float)); CUDA_CHK(cerr);

  for (int i = 1; i < numDevices; i++) {
    cBlock[i].rowStart = M;
    cBlock[i].rowEnd   = M;
    cBlock[i].colStart = N;
    cBlock[i].colEnd   = N;
    aBlock[i].rowStart = M;
    aBlock[i].rowEnd   = M;
    aBlock[i].colStart = R;
    aBlock[i].colEnd   = R;
    bBlock[i].rowStart = R;
    bBlock[i].rowEnd   = R;
    bBlock[i].colStart = N;
    bBlock[i].colEnd   = N;
    CLocal_p[i] = NULL;
    ALocal_p[i] = NULL;
    BLocal_p[i] = NULL;
  }

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysSingleDevice(MMMA mmma, size_t M, size_t N, size_t R,
    int numDevices,
    struct MatrixBlock cBlock[],
    struct MatrixBlock aBlock[],
    struct MatrixBlock bBlock[],
    float *CLocal_p[], float *ALocal_p[], float *BLocal_p[])
{
  cudaError_t    cerr;

  PetscFunctionBeginUser;
  cerr = cudaFree(CLocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(ALocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(BLocal_p[0]); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}

int MMMAApplyDoubleDevice(MMMA, size_t M, size_t N, size_t R, double alpha, int numDevices,
    const struct MatrixBlock cBlock[],
    const struct MatrixBlock aBlock[],
    const struct MatrixBlock bBlock[],
    double *Clocal[], const double *Alocal[], const double *Blocal[])
{
  return 0;
}
  __global__ void
gpu_mmma(const size_t M, const size_t R, const size_t N, float alpha, const float * A, const float *B, float * C)
{
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int t = tx * BLOCK_K + ty;
  int num_iter = (R + BLOCK_K - 1) / BLOCK_K;
  const float2* B2 = (float2 *)B;
  //int global_row_base_A = BLOCK_ROW * bx + STRIDES * tx;
  int global_col_B = BLOCK_COL * blockIdx.y + 2 * t;
  bool B_aligned = R % 2 == 0;
  float2 Cs[BLOCK_ROW];
  float2 zero;
  zero.x = 0.0;
  zero.y = 0.0;
  for (int i = 0; i < BLOCK_ROW; ++i) {
    Cs[i] = zero;
  }
  __shared__ float As[BLOCK_K][BLOCK_ROW];
  for (int iter = 0; iter < num_iter; ++iter) {
    int global_col = 32 * iter + ty;
    for (int j = 0; j < STRIDES; ++j) {
      int global_row = BLOCK_ROW * blockIdx.x + STRIDES * tx + j;
      int global_index = global_row * R + global_col;
      int local_row = STRIDES * tx + j;
      As[ty][local_row] = A[global_index];
    }

    __syncthreads();

    int global_row_B = BLOCK_K * iter;
    int global_index = global_row_B * N + global_col_B;
    float2 Bs;
    for (int j = 0; j < BLOCK_K; ++j, global_index += N, global_row_B++) {

      if (global_row_B >= R) break;
      if (B_aligned) {
        Bs = B2[global_index / 2];
      }
      else {
        Bs.x = B[global_index];
        Bs.y = B[global_index + 1];
      }
      if (global_col_B >= N) Bs.x = 0.0;
      if (global_col_B >= N) Bs.y = 0.0;
      for (int i = 0; i < BLOCK_ROW; ++i) {
        Cs[i].x += As[j][i] * Bs.x;
        Cs[i].y += As[j][i] * Bs.y;
      }
    }
    __syncthreads();
  }
  int row_C = BLOCK_ROW * blockIdx.x;
  int global_index_C = row_C * N + global_col_B;
#pragma unroll
  for (int i = 0; i < BLOCK_ROW; ++i, ++row_C, global_index_C += N) {
    if (row_C < M) {
      if (global_col_B + 1 < N) {
        C[global_index_C] *= alpha;
        C[global_index_C] += Cs[i].x;
        C[global_index_C + 1] *= alpha;
        C[global_index_C + 1] += Cs[i].y;
      }
      else if (global_col_B < N) {
        C[global_index_C] *= alpha;
        C[global_index_C]  += Cs[i].x;
      }
    }
  }
}
int MMMAApplySingleDevice(MMMA, size_t M, size_t N, size_t R, float alpha, int numDevices,
    const struct MatrixBlock cBlock[],
    const struct MatrixBlock aBlock[],
    const struct MatrixBlock bBlock[],
    float *Clocal[], const float *Alocal[], const float *Blocal[])
{
  dim3 grid_dim((M + BLOCK_ROW - 1)/BLOCK_ROW, (N + BLOCK_COL - 1) / BLOCK_COL); 
  dim3 block_dim(BLOCK_COL / 2 / BLOCK_K, BLOCK_K);
  gpu_mmma <<<grid_dim, block_dim>>> (M, R, N, alpha, Alocal[0], Blocal[0], Clocal[0]);
  return 0;
}
/* vi: set expandtab sw=2 ts=2 cindent: */
